jQuery( function( $ ) {


	var nbt_ajax_cart_load = {
		/**
		 * Init jQuery.BlockUI
		 */
		block: function($el) {
			$el.block({
				message: null,
				overlayCSS: {
					background: '#fff',
					opacity: 0.6
				}
			});
		},

		/**
		 * Remove jQuery.BlockUI
		 */
		unblock: function($el) {
			$el.unblock();
		}
	}
	var nbt_ajax_cart = {

		/**
		 * Initialize variations actions
		 */
		init: function() {
        	$(document).on('click', '.add_to_cart_button', this.add_to_cart);
        	$(document).on('click', '.nbt-ac-icon', this.show_cart_content);
        	$(document).on('click', '.mini_cart_item .remove', this.remove_to_cart);
        	$(document).on('click', '.single_add_to_cart_button:not(.disabled)', this.add_to_cart_single);

			$(".cart_list").mCustomScrollbar({
				theme:"dark"
			});

			this.load_cal();

			$(document).mouseup(function(e) 
			{
				if($(".nbt-mini-cart").length){
				    var container = $(".nbt-mini-cart");
				    var icon = $(".nbt-ac-icon");

				    if (!container.is(e.target) && container.has(e.target).length === 0 && !icon.is(e.target) && icon.has(e.target).length === 0) 
				    {
						container.hide();
						container.removeClass('open');
				    }

				    if(icon.is(e.target)){

				    }
				}

			});

		},
		show_cart_content: function(){
			var $el = $(this).next();
			if($el.hasClass('open')){
				$el.hide();
				$el.removeClass('open');
			}else{
				$el.show();
				$el.addClass('open');
			}
		},
		load_cal: function(){
			if($(".nbt-mini-cart").length){
				var $width = $(window).width() - 300;
				var position = $('.nbt-ajax-cart').offset();
			
				if($width < position.left){
					$(".nbt-mini-cart").addClass('nbt-ajaxcart-right');
					$(".nbt-mini-cart").css({right: 0});
				}else{
					$(".nbt-mini-cart").addClass('nbt-ajaxcart-left');
					$(".nbt-mini-cart").css({left: 0});
				}
			}
		},
		add_to_cart: function(){

			var $li = $(this).closest('li');
			nbt_ajax_cart_load.block($li);
			if( !$(this).hasClass('product_type_variable') ){
				$('.nbt-cart-list-wrap ul').remove();
				$.ajax({
					url: wc_add_to_cart_params.ajax_url,
					data: {
						action:     'nbt_add_to_cart',
						product_id: $(this).attr('data-product_id')
					},
					type: 'POST',
					datatype: 'json',
					success: function( response ) {
						nbt_ajax_cart_load.unblock($li);
						var rs = JSON.parse(response);
						if ( rs.complete != undefined ) {
							$('.nbt-ac-count').text(rs.count);
							$('.nbt-mini-cart-wrap').html(rs.html);
							$(".cart_list").mCustomScrollbar({
							    theme: "dark"
							});

							if(rs.url_checkout != undefined){
								$('.nbt-mini-cart-wrap .wc-forward').attr('href', rs.url_checkout);
							}
							if(rs.error != undefined){
								alert(rs.title);
							}else{
								nbt_ajax_cart.notification(rs);
							}
							
						}else{
							window.location.href = rs.url;
						}
						

					},
					error:function(){
						alert('There was an error when processing data, please try again !');
						nbt_ajax_cart_load.unblock($li);
					}
				});
				return false;
			}

		},
		remove_to_cart: function(){
			var $this = $(this).closest('li');
			nbt_ajax_cart_load.block($('.nbt-mini-cart'));
			$.ajax({
				url: wc_add_to_cart_params.ajax_url,
				data: {
					action:     'nbt_remove_to_cart',
					product_id: $(this).attr('data-product_id'),
					variation_id: $(this).attr('data-variation_id')
				},
				type: 'POST',
				datatype: 'json',
				success: function( response ) {
					var rs = JSON.parse(response);
					if ( rs.complete != undefined ) {
						$this.remove();
						if( !rs.count ){
							$('.nbt-mini-cart-wrap').html(rs.html);
						}
						$('.nbt-ac-count').text(rs.count);
						$('.nbt-mini-cart-wrap .total').html(rs.subtotal);
					}
					nbt_ajax_cart_load.unblock($('.nbt-mini-cart'));
				},
				error:function(){
					alert('There was an error when processing data, please try again !');
					nbt_ajax_cart_load.unblock();
				}
			});
			return false;
		},
		add_to_cart_single: function(){
			var $this = $(this);
			nbt_ajax_cart_load.block($this);
			$('.nbt-cart-list-wrap ul').remove();

			if($('body').hasClass('single-product')){
				product_id = $('[name="add-to-cart"]').val();
				variation_id = $('[name="variation_id"]').val()
			}else{
				product_id = $this.val();
				variation_id = '';
			}


			$.ajax({
				url: wc_add_to_cart_params.ajax_url,
				data: {
					action:     'nbt_add_to_cart_single',
					product_id: product_id,
					variation_id: variation_id,
					quantity: $('[name="quantity"]').val()
				},
				type: 'POST',
				datatype: 'json',
				success: function( response ) {

					var rs = JSON.parse(response);
					if ( rs.complete != undefined ) {
						$('.nbt-ac-count').text(rs.count);
						$('.nbt-mini-cart-wrap').html(rs.html);
						$('.nbt-mini-cart-wrap .total').html(rs.subtotal);
						
						$(".cart_list").mCustomScrollbar({
						    theme: "dark"
						});
						if(rs.error != undefined){
							alert(rs.title);
						}else{
							if(rs.url_checkout != undefined){
								window.location.href = rs.url_checkout;
							}else{
								nbt_ajax_cart.notification(rs);
							}
						}
						if(rs.url_checkout != undefined){
							$('.nbt-mini-cart-wrap .wc-forward').attr('href', rs.url_checkout);
						}
					}
					nbt_ajax_cart_load.unblock($this);

				},
				error:function(){
					alert('There was an error when processing data, please try again !');
					nbt_ajax_cart_load.unblock($this);
				}
			});

			return false;
		},
		notification: function(rs){
			$('.ajaxcart-notification').remove();

			$('body').append('<div id="growls" class="ajaxcart-notification default" style="top: ' + rs.top + 'px;"><div class="growl growl-notice growl-medium"><div class="growl-message"><div class="growl-close">×</div>' + rs.title + '</div></div>');

			$('.ajaxcart-notification').hide().show('slow').delay(2000).hide('slow');
		}
	}


	nbt_ajax_cart.init();
	
});