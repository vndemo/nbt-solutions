<?php
class NBT_WooCommerce_AjaxCart_Admin{

	
	public function __construct() {

		add_action( 'admin_enqueue_scripts', array($this, 'nbt_ajaxcart_scripts_method') );
        if( defined('PREFIX_NBT_SOL') && !class_exists('NBT_Plugins') ){
            //add_action('admin_menu', array($this, 'register_subpage_media'));
        }else{

            if( !class_exists('NBT_Plugins') ){
                require_once AJAX_CART_PATH . 'inc/plugins.php';
            }
            add_action( 'admin_menu', array( $this, 'register_panel' ), 5 );
        }

        add_filter('nbt_admin_field_icon', array($this, 'admin_icon'), 10, 3 );

	}

    public function register_panel(){
        $args = array(
            'create_menu_page' => true,
            'parent_slug'   => '',
            'page_title'    => __( 'Ajax Cart', 'nbt-plugins' ),
            'menu_title'    => __( 'Ajax Cart', 'nbt-plugins' ),
            'capability'    => apply_filters( 'nbt_cs_settings_panel_capability', 'manage_options' ),
            'parent'        => '',
            'parent_page'   => 'ntb_plugin_panel',
            'page'          => 'nbt-ajax-cart',
            'admin-tabs'    => $this->available_tabs,
            'functions'     => array(__CLASS__ , 'ntb_cs_page'),
            'font-path'  => AJAX_CART_URL . 'assets/css/nbt-plugins.css'
        );

        $this->_panel = new NBT_Plugins($args);
    }

    public function ntb_cs_page(){
        include(AJAX_CART_PATH .'tpl/admin.php');
    }

    public function admin_icon($html, $field, $value){

        ob_start();
        ?>
        <tr valign="top">
            <th scope="row" class="titledesc">
                <label for="<?php echo esc_attr( $field['id'] ); ?>"><?php echo esc_html( $field['name'] ); ?></label>
            </th>
            <td class="forminp forminp-<?php echo sanitize_title( $field['type'] ) ?>">
                <ul class="<?php echo esc_attr( $field['id'] ); ?>">
                    <?php foreach ($field['options'] as $k => $icon) {?>
                    <li<?php if($value == $icon){ echo ' class="active"';}?> data-icon="<?php echo $icon;?>">
                        <i class="<?php echo $icon;?>"></i>
                        <input type="radio" name="<?php echo esc_attr( $field['id'] ); ?>" value="<?php echo $icon;?>"<?php if($value == $icon){ echo ' checked';}?> />
                    </li>
                    <?php }?>
                </ul>
            </td>
        </tr>
        <?php $html = ob_get_clean();

        return $html;
    }

	public function nbt_ajaxcart_scripts_method($hooks){
		wp_enqueue_style( 'ntb-fonts', AJAX_CART_URL . 'assets/css/ntb-fonts.css'  );
		wp_enqueue_style( 'admin', AJAX_CART_URL . 'assets/css/admin.css'  );
        wp_enqueue_script( 'admin', AJAX_CART_URL . 'assets/js/admin.js', null, null, true );
	}
}
new NBT_WooCommerce_AjaxCart_Admin();