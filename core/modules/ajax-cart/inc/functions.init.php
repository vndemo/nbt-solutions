<?php
	function nbt_ajax_template(){
		$ajaxcart_settings = get_option('ajax-cart_settings');
		$get_settings = NBT_Ajax_Cart_Settings::get_settings();

		$ajaxcart_icon = $ajaxcart_settings['wc_ajax_cart_icon'];
		if(!$ajaxcart_icon){
			if(isset($get_settings['icon']['default'])){
				$ajaxcart_icon = $get_settings['icon']['default'];
			}
		}

		$ajaxcart_color_icon = $ajaxcart_settings['wc_ajax_cart_color_icon'];
		if(!$ajaxcart_color_icon){
			if(isset($get_settings['icon_color']['default'])){
				$ajaxcart_color_icon = $get_settings['icon_color']['default'];
			}
		}

		$ajaxcart_color_count = $ajaxcart_settings['wc_ajax_cart_color_count'];
		if(!$ajaxcart_color_count){
			if(isset($get_settings['icon_bg']['default'])){
				$ajaxcart_color_count = $get_settings['icon_bg']['default'];
			}
		}

		$ajaxcart_color_count_text = $ajaxcart_settings['wc_ajax_cart_color_count_text'];
		if(!$ajaxcart_color_count_text){
			if(isset($get_settings['count_color_text']['default'])){
				$ajaxcart_color_count_text = $get_settings['count_color_text']['default'];
			}
		}


		$ajaxcart_primary_color = $ajaxcart_settings['wc_ajax_cart_primary_color'];
		if(!$ajaxcart_primary_color){
			if(isset($get_settings['primary_color']['default'])){
				$ajaxcart_primary_color = $get_settings['primary_color']['default'];
			}
		}


		?>
		<style type="text/css">
			.nbt-ac-icon i{
				color: <?php echo $ajaxcart_color_icon;?>;
			}
			.nbt-ac-icon .nbt-ac-count{
				background-color: <?php echo $ajaxcart_color_count;?>;
				color: <?php echo $ajaxcart_color_count_text;?>;
			}
			.nbt-mini-cart{
				border-color: <?php echo $ajaxcart_primary_color;?>;
			}
			.nbt-mini-cart:before{
				border-bottom-color: <?php echo $ajaxcart_primary_color;?>;
			}

		</style>

		<div class="nbt-ajax-cart">
			<div class="nbt-ac-icon">
				<?php echo filter_nbt_cart_icon();?>
			</div>
			
			<div class="nbt-mini-cart">
				<div class="nbt-mini-cart-wrap">
					<?php
					if( WC()->cart->get_cart_contents_count() ){?>
						<div class="nbt-cart-list-wrap">
							<ul class="cart_list">
								<?php template_loop();?>

							</ul>
						</div>
						<?php if ( ! WC()->cart->is_empty() ) : ?>
							<p class="total"><strong><?php _e( 'Subtotal', 'woocommerce' ); ?>:</strong> <?php echo WC()->cart->get_cart_subtotal(); ?></p>

							<?php do_action( 'woocommerce_widget_shopping_cart_before_buttons' ); ?>

							<p class="buttons">
								<?php do_action( 'woocommerce_widget_shopping_cart_buttons' ); ?>
							</p>
						<?php endif; ?>
					<?php }else{?>
						<p class="woocommerce-mini-cart__empty-message"><?php _e( 'No products in the cart.', 'woocommerce' ); ?></p>
					<?php }?>
				</div>

			</div>
		</div>
		<?php
	}

	function filter_nbt_cart_icon(){
		$ajaxcart_settings = get_option('ajax-cart_settings');
		$ajaxcart_icon = $ajaxcart_settings['wc_ajax_cart_icon'];
		$get_settings = NBT_Ajax_Cart_Settings::get_settings();
		if(!$ajaxcart_icon){
			if(isset($get_settings['icon']['default'])){
				$ajaxcart_icon = $get_settings['icon']['default'];
			}
		}


		$price = WC()->cart->get_cart_total();
		$total_count = WC()->cart->get_cart_contents_count();
		if($total_count == 0){
			$count = wp_kses_data( sprintf( _n( '%d item', '%d item', $total_count, 'nbt-ajax-cart' ), $total_count ) );
		}else{
			$count = wp_kses_data( sprintf( _n( '%d item', '%d items', $total_count, 'nbt-ajax-cart' ), $total_count ) );
		}

		return apply_filters( 'nbt_cart_icon', 'Old', $ajaxcart_icon, $count, $price );
	}

	function template_loop($is_ajax = false){
		if( WC()->cart->get_cart_contents_count() ){
		$items = WC()->cart->get_cart();
		foreach ($items as $cart_item_key => $cart_item) {
			$product_id = $cart_item['product_id'];
			$variation_id = $cart_item['variation_id'];


			$attr_variation = $variation_id ? ' data-variation_id="'.$variation_id.'"' : '';
			$_product = wc_get_product( $variation_id ? $variation_id : $product_id );
			$thumb = get_the_post_thumbnail( $cart_item['product_id'], array('50', '50') );



			?>
		<li class="mini_cart_item">
			<div class="nbt-ac-left">
				<a href="<?php echo get_permalink($cart_item['product_id']);?>" title="<?php echo $_product->get_name();?>"><?php echo $thumb;?></a>
			</div>

			<div class="nbt-ac-right">
				<h4><a href="<?php echo get_permalink($cart_item['product_id']);?>" title="<?php echo $_product->get_name();?>"><?php echo $_product->get_name();?></a></h4> 
			<?php

			$rating_count = $_product->get_rating_count();
			$review_count = $_product->get_review_count();
			$average      = $_product->get_average_rating();

			if ( $rating_count > 0 ) : ?>
				<div class="woocommerce-product-rating">
					<?php echo wc_get_rating_html( $average, $rating_count ); ?>
				</div>
			<?php else: ?>
				<div class="woocommerce-product-rating">
					<div class="star-rating" title="Rated 0.00 out of 5"><span style="width:0"><strong class="rating">0.00</strong> out of 5</span></div>
				</div>
			<?php endif;
			
		    if($_product->get_price_html() !=''):
		        echo '<span class="product-price sr-price">';
					echo ac_get_price($cart_item['data']->get_price(), $is_ajax);
		        echo '</span> × '.$cart_item['quantity'];
		    endif;
		    ?>
			<?php
			echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
				'<a href="%s" class="remove" aria-label="%s" data-product_id="%s"%s data-product_sku="%s">&times;</a>',
				esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
				__( 'Remove this item', 'woocommerce' ),
				esc_attr( $product_id ),
				$attr_variation,
				esc_attr( $_product->get_sku() )
			), $cart_item_key );
			?>
			</div>
		</li>
		<?php }
		}
	}

	function ac_get_price($price, $is_ajax = false){
		global $currency;

		
		$ajaxcart_rate = 1;
		$ajaxcart_pos = '%1$s%2$s';
		$ajaxcart_symbol = get_woocommerce_currency_symbol(get_option('woocommerce_currency'));
		$ajaxcart_decimals = get_option('woocommerce_price_num_decimals' );
		$ajaxcart_currency = get_option('woocommerce_currency');
		if(isset($currency) && is_array($currency)){
			

			$ajaxcart_rate = $currency['nbt_currency-switcher_rates'];
			$ajaxcart_position = $currency['nbt_currency-switcher_position'];
			$ajaxcart_decimals = $currency['nbt_currency-switcher_decimals'];
			$ajaxcart_symbol = $currency['nbt_currency-switcher_repeater_symbol'];
			$ajaxcart_currency = $currency['nbt_currency-switcher_repeater_currency'];




			switch ($ajaxcart_position) {
			    case 'left' :
				$ajaxcart_pos = '%1$s%2$s';
				break;
			    case 'right' :
				$ajaxcart_pos = '%2$s%1$s';
				break;
			    case 'left_space' :
				$ajaxcart_pos = '%1$s&nbsp;%2$s';
				break;
			    case 'right_space' :
				$ajaxcart_pos = '%2$s&nbsp;%1$s';
				break;
			}
		}


		$array_position = str_replace('&nbsp;', '', $ajaxcart_pos);

        if ( $price ) {

        	if($is_ajax){
        		$price = round($price * $ajaxcart_rate);
        		$price = '<span class="woocommerce-Price-amount amount">'.str_replace(array('%2$s', '%1$s'), array($price, '<span class="woocommerce-Price-currencySymbol">'.$ajaxcart_symbol.'</span>'), $array_position).'</span>';
        	}else{
	  			$price = wc_price($price, array(
	        		'decimals' => $ajaxcart_decimals,
	        		'price_format' => $ajaxcart_pos,
	        		'currency' => $ajaxcart_currency,
	        	));
        	}



        	return $price;
        }
	}

?>