<?php
class NBT_Ajax_Search_Settings{

	protected static $initialized = false;

	public static function initialize() {
		// Do nothing if pluggable functions already initialized.
		if ( self::$initialized ) {
			return;
		}


		// State that initialization completed.
		self::$initialized = true;
	}

    public static function get_settings() {
        $settings = array(
            'icon_color' => array(
                'name' => __( 'Color Icons', 'nbt-ajax-cart' ),
                'type' => 'color',
                'id'   => 'wc_'.NBT_Solutions_Ajax_Search::$plugin_id.'_color_icon',
                'default' => '#444'
            ),
            'primary_color' => array(
                'name' => __( 'Primary Color', 'nbt-ajax-cart' ),
                'type' => 'color',
                'id'   => 'wc_'.NBT_Solutions_Ajax_Search::$plugin_id.'_primary_color',
                'default' => '#25bce9'
            )
        );
        return apply_filters( 'nbt_'.NBT_Solutions_Ajax_Search::$plugin_id.'_settings', $settings );
    }

}
