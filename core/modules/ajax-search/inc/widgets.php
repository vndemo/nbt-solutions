<?php
class NBT_Ajax_Search_Widget extends WP_Widget {
	public function __construct() {
		parent::__construct(
			'nbt-wc-ajaxsearch',
			__( 'NBT Ajax Search', 'nbt-ajax-cart' ),
			array(
				'classname' => 'nbt-wc-ajaxsearch',
				'description' => __( 'Enter a custom description for your new widget', 'mycustomdomain' )
			)
		);
	}

	public function form( $instance ) {
		$title = esc_attr( $instance['title'] );?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?>: 
				<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>">
			</label>
		</p>
	<?php
	}

	public function widget( $args, $instance ) {
		global $woocommerce;
		extract( $args );

		$title = apply_filters( 'widget_title', $instance['title'] );
		$paragraph = $instance['paragraph'];

		echo $before_widget;
			if ( $title ) :
				echo $before_title . $title . $after_title;
			endif;

			echo '<i class="fa fa-search" aria-hidden="true" style="display: block;"></i>';

		echo $after_widget;

	}

}
