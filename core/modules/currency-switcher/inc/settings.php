<?php
class NBT_Currency_Switcher_Settings{

	protected static $initialized = false;

	public static function initialize() {
		// Do nothing if pluggable functions already initialized.
		if ( self::$initialized ) {
			return;
		}


		// State that initialization completed.
		self::$initialized = true;
	}

    public static function get_settings() {
        $etalon_new = array();
        for ($etalon = 0; $etalon <= 8; $etalon++) {
            $etalon_new[$etalon] = $etalon;
        }

        $new_currency = array(0 => '(Select an Currency)');
        foreach (get_woocommerce_currencies() as $code => $name) {
            $new_currency[$code] = $name . ' (' . get_woocommerce_currency_symbol( $code ) . ')';
        }
        $settings = array(
            'cron_time' => array(
                'name' => __( 'Hour to Update', 'nbt-ajax-cart' ),
                'type' => 'number',
                'id'   => 'nbt_'.NBT_Solutions_Currency_Switcher::$plugin_id.'_cron_time',
                'default' => '3600',
                'min' => 3600
            ),
            'repeater' => array(
                'name' => __( 'List of currencies', 'nbt-ajax-cart' ),
                'type' => 'repeater',
                'desc'     => __( 'Start typing the Custom Tab name, Used for including custom tabs on all products.', 'GWP' ),
                'id' => 'nbt_'.NBT_Solutions_Currency_Switcher::$plugin_id.'_repeater',
                'temp' => NBT_MCS_PATH . 'tpl/admin/settings.php',
                'fields' => array(
                    'currency' => array(
                        'name' => __( 'Currency', 'nbt-ajax-cart' ),
                        'type' => 'select',
                        'class' => 'mcs-select2',
                        'id'   => 'nbt_'.NBT_Solutions_Currency_Switcher::$plugin_id.'_repeater_currency',
                        'default' => '',
                        'options' => $new_currency
                    ), 
                    'position' => array(
                        'name' => __( 'Position', 'nbt-ajax-cart' ),
                        'type' => 'select',
                        'id'   => 'nbt_'.NBT_Solutions_Currency_Switcher::$plugin_id.'_position',
                        'default' => '',
                        'options' => NBT_Solutions_Currency_Switcher::symbol_position()
                    ), 
                    'decimals' => array(
                        'name' => __( 'Decimals', 'nbt-ajax-cart' ),
                        'type' => 'select',
                        'id'   => 'nbt_'.NBT_Solutions_Currency_Switcher::$plugin_id.'_decimals',
                        'default' => '3600',
                        'options' => $etalon_new
                    ), 
                    'rates' => array(
                        'name' => __( 'Rates', 'nbt-ajax-cart' ),
                        'type' => 'rate',
                        'id'   => 'nbt_'.NBT_Solutions_Currency_Switcher::$plugin_id.'_rates',
                        'default' => '0',
                        'min' => 0
                    ), 
                )
            )
        );
        return apply_filters( 'nbt_'.NBT_Solutions_Currency_Switcher::$plugin_id.'_settings', $settings );
    }




}
