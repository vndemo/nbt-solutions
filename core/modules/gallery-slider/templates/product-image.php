<div class="images twist-wrap<?php if(isset($settings['wc_gallery_slider_direction'])){echo ' nbt-gallery-'.$settings['wc_gallery_slider_direction'];}?>">
	<div class="twist-pgs">
		<?php if ( $attachment_ids && has_post_thumbnail() ) {
			$thumbnail_id = get_post_meta( $post->ID, '_thumbnail_id', true );
			$full_size_image_thumb = wp_get_attachment_image_src( $thumbnail_id, 'full' );
			$image_title     = get_post_field( 'post_excerpt', $thumbnail_id );
			$thumb_attributes = array(
				'class' => 'attachment-shop_single size-shop_single wp-post-image',
				'title'                   => $image_title,
				'data-src'                => $full_size_image[0],
				'data-large_image'        => $full_size_image[0],
				'data-large_image_width'  => $full_size_image[1],
				'data-large_image_height' => $full_size_image[2],
			);?>
			<div class="woocommerce-product-gallery__image wc-product-main-image">
				<a  class="venobox" href="<?php echo $full_size_image[0];?>" data-title="<?php echo $image_title;?>" data-gall="pgs-thumbs" >
					<?php echo wp_get_attachment_image( $thumbnail_id, 'shop_single', false, $thumb_attributes );?>
				</a>
			</div>
			<?php
			foreach ( $attachment_ids as $k => $attachment_id ) {
				$full_size_image = wp_get_attachment_image_src( $attachment_id, 'full' );
				$thumbnail       = wp_get_attachment_image_src( $attachment_id, 'shop_thumbnail' );
				$image_title     = get_post_field( 'post_excerpt', $attachment_id );
				$thumb_attributes = array(
					'class' => 'attachment-shop_single size-shop_single wp-post-image',
					'title'                   => $image_title,
					'data-src'                => $full_size_image[0],
					'data-large_image'        => $full_size_image[0],
					'data-large_image_width'  => $full_size_image[1],
					'data-large_image_height' => $full_size_image[2],
				);
				?>
			<div class="woocommerce-product-gallery__image">
				<a  class="venobox" href="<?php echo $full_size_image[0];?>" data-title="<?php echo $image_title;?>" data-gall="pgs-thumbs" >
					<?php echo wp_get_attachment_image( $attachment_id, 'shop_single', false, $thumb_attributes );?>
				</a>
			</div>
				<?php
			}
		}?>
	</div>
	<div class="slider-nav" id="slide-nav-pgs">
		<?php if ( $attachment_ids && has_post_thumbnail() ) {
			$thumbnail_id = get_post_meta( $post->ID, '_thumbnail_id', true );
			$full_size_image_thumb = wp_get_attachment_image_src( $thumbnail_id, 'shop_thumbnail' );
			$image_title     = get_post_field( 'post_excerpt', $thumbnail_id );
			$thumb_attributes = array(
				'class' => 'attachment-shop_single size-shop_single wp-post-image',
				'title'                   => $image_title,
				'data-src'                => $full_size_image[0],
				'data-large_image'        => $full_size_image[0],
				'data-large_image_width'  => $full_size_image[1],
				'data-large_image_height' => $full_size_image[2],
			);

			$newWidth = 157;
			$newHeight = 157;

			$first_thumb = preg_replace(
			   array('/width="\d+"/i', '/height="\d+"/i'),
			   array(sprintf('width="%d"', $newWidth), sprintf('height="%d"', $newHeight)),
			   wp_get_attachment_image( $thumbnail_id, 'shop_single', false, $thumb_attributes ));

			?>
			<div>
				<a class="product-gallery__image_thumb" data-title="<?php echo $image_title;?>" data-gall="pgs-thumbs" data-href="<?php echo $full_size_image[0];?>"><img width="157" height="157" src="<?php echo $full_size_image_thumb[0];?>"></a>
			</div>
			<?php
			foreach ( $attachment_ids as $k => $attachment_id ) {
				$thumbnail       = wp_get_attachment_image_src( $attachment_id, 'thumbnail' );
				$image_title     = get_post_field( 'post_excerpt', $attachment_id );
				?>
				<div>
					<a href="javascript:;" data-title="<?php echo $image_title;?>" data-gall="pgs-thumbs"><img src="<?php echo $thumbnail[0];?>" ></a>
				</div>

				<?php
			}
		}?>
	</div>


</div>


<script>
jQuery.noConflict();
(function( $ ) {
  $(function() {
    // More code using $ as alias to jQuery
        
    
        $(document).ready(function(){



	    $('.venobox').venobox({
	    	framewidth: '800px',
	    	autoplay: true,
	    	titleattr: 'data-title',
	    	titleBackground: '#000000',
	    	titleBackground: '#000000',
	    	titleColor: '#fff',
	    	numerationColor: '#fff',
	    	arrowsColor: '5',
	    	titlePosition: 'bottom',
	    	numeratio: true,
	    	spinner : 'double-bounce',
	    	spinColor: '#fff',
	    	border: '5px',
	    	bgcolor: '#000000',
	    	infinigall: false,
	    	numerationPosition: 'bottom'
	    });
	   
	    // go to next item in gallery clicking on .next

     

	  
		  $('.twist-pgs').slick({
  		  accessibility: false,//prevent scroll to top
  		  lazyLoad: 'progressive',
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  arrows: true,
		  fade: false,
		  swipe :true,
   		 
		 
		  	   		prevArrow: '<i class="btn-prev dashicons dashicons-arrow-left-alt2"></i>',
		  	nextArrow: '<i class="btn-next dashicons dashicons-arrow-right-alt2"></i>',
		  		  rtl: false,
		  infinite: false,
		  autoplay: true,
		  pauseOnDotsHover: true,
		  autoplaySpeed: '5000',
		  		  asNavFor: '#slide-nav-pgs',
		  		  dots :false,
  
			});

		  		

	    $('#slide-nav-pgs').slick({
			accessibility: false,//prevent scroll to top
			isSyn: false,//not scroll main image

		  slidesToShow: 4,
		  slidesToScroll: 4,
		  infinite: false,
		  asNavFor: '.twist-pgs',
		  prevArrow: '<i class="btn-prev dashicons dashicons-arrow-left-alt2"></i>',
		  nextArrow: '<i class="btn-next dashicons dashicons-arrow-right-alt2"></i>',
		  dots: false,
		  centerMode: false,
	   	  rtl: false,
	   	  <?php if(empty($settings) || isset($settings['wc_gallery_slider_direction']) && $settings['wc_gallery_slider_direction'] == 'vertical'){
	   	  		echo 'vertical: true,';
	   	  }?>
		  

		  draggable: true,
		  focusOnSelect: true,

		 responsive: [
		    {
		      breakpoint: 767,
		      settings: {
		        slidesToShow: 3,
		        slidesToScroll: 3,
		        vertical: false,
		        autoplay: false,//no autoplay in mobile
				isMobile: true,// let custom knows on mobile
				arrows: false //hide arrow on mobile
		      }
		    },
		    ]
		});

	    

		
	
	
	$('.woocommerce-product-gallery__image img').load(function() {

	    var imageObj = $('.woocommerce-product-gallery__image a');
	    if (!(imageObj.width() == 1 && imageObj.height() == 1)) {
	  
	    	$('.twist-pgs .woocommerce-product-gallery__image , #slide-nav-pgs .slick-slide .product-gallery__image_thumb').trigger('click');
	   		$('.woocommerce-product-gallery__image img').trigger('zoom.destroy');
	   		$('.woocommerce-product-gallery__image img').wrap('<span class="slider-inline-block" style="display:inline-block"></span>').parent().zoom({touch: false});

	   		/* Get first image */
	   		var src = $('.woocommerce-product-gallery__image.slick-active img').attr('src');
	   		$('.slider-nav .slick-current img').attr('src', src);
	   	
	   		
	    }
	});
	 

	});
  });
})(jQuery);	


</script>

<style>

	.twist-pgs {
	 width: 79%;
    float: right;
    margin-left: 1%;

}
@media only screen and (max-width: 767px) {
   .twist-pgs {
		 width: 100%;
	    float: none;
	    margin-left: 0%;

	}
	.slider-nav .btn-next,.slider-nav .btn-prev{
		margin: 0px;
	}
}



	.twist-video-thumb:after{
		color: #e54634;
	}

	.btn-prev, .btn-next{
		color: #fff;
		background:#000000;
	}
	.slick-prev:before, .slick-next:before{
		color: #fff;
		
	}

</style>