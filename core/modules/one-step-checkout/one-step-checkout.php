<?php
/**
 * @version    1.0
 * @package    Package Name
 * @author     Your Team <support@yourdomain.com>
 * @copyright  Copyright (C) 2014 yourdomain.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 */

/**
 * Plug additional sidebars into WordPress.
 *
 * @package  Package Name
 * @since    1.0
 */
define('NBT_OSC_PATH', plugin_dir_path( __FILE__ ));
define('NBT_OSC_URL', plugin_dir_url( __FILE__ ));

class NBT_Solutions_One_Step_Checkout {
    /**
     * Variable to hold the initialization state.
     *
     * @var  boolean
     */
    protected static $initialized = false;

    public static $types = array();
    
    /**
     * Initialize functions.
     *
     * @return  void
     */
    public static function initialize() {
        // Do nothing if pluggable functions already initialized.
        if ( self::$initialized ) {
            return;
        }

        if ( ! function_exists( 'WC' ) ) {
            add_action( 'admin_notices', array( __CLASS__, 'install_woocommerce_admin_notice') );
        }else{

            add_action( 'init', array( __CLASS__, 'check_init' ), 9999 );


     

            if(!is_admin()){
                require_once 'inc/frontend.php';
            }


        }
        // Register actions to do something.
        //add_action( 'action_name'   , array( __CLASS__, 'method_name'    ) );
        // State that initialization completed.
        self::$initialized = true;
    }



    public static function check_init(){

        $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $page_id = url_to_postid($actual_link);
        if(wc_get_page_id( 'cart' ) == $page_id){
            $option = get_option('one-step-checkout_settings');
            if(isset($option['wc_one_step_checkout_redirect_cart']) && $option['wc_one_step_checkout_redirect_cart']){
                wp_redirect(wc_get_checkout_url());
                die();
            }
        }

    }
}