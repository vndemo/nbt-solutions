		jQuery(document).ready(function($){
			var x = false;
			var nbtou_load = {
				/**
				 * Init jQuery.BlockUI
				 */
				block: function($el) {
					$el.block({
						message: null,
						overlayCSS: {
							background: '#fff',
							opacity: 0.6
						}
					});
				},

				/**
				 * Remove jQuery.BlockUI
				 */
				unblock: function($el) {
					$el.unblock();
				}
			}
			var nbtou_js = {
				init: function(){


					$(document).on('dragover', '.nbt-upload-zone', this.dragover_files);
					$(document).on('dragleave', '.nbt-upload-zone', this.dragleave_files);
					$(document).on('drop', '.nbt-upload-zone', this.drop_files);
					$(document).on('click', '.nbt-oupload-target', this.click_files);
					$(document).on('change', '.nbt-upload-input', this.change_files);
					$(document).on('click', '.nbt-icon-cancel', this.remove_files);
					$(document).on('click', '.toggle-order-upload', this.toggle_order_upload);
	

				},
				toggle_order_upload: function(){
					if($(this).hasClass('active')){
						$(this).next().slideUp();
						$(this).removeClass('active');
					}else{
						$(this).next().slideDown();
						$(this).addClass('active');
					}
				},
				dragover_files: function(e){
				    e.preventDefault();
				    e.stopPropagation();
				    $(this).addClass('dragover');
				},
				dragleave_files: function(e){
				    e.preventDefault();
				    e.stopPropagation();
				    $(this).removeClass('dragover');
				},
				drop_files: function(e){
				    e.preventDefault();
				    e.stopPropagation();
				    $(this).removeClass('dragover');

				    nbtou_js.triggerCallback(e);

				},
				triggerCallback: function(e){
					var $product_id = $('[name="add-to-cart"]').val();
					var files;
					if(e.originalEvent.dataTransfer) {
						files = e.originalEvent.dataTransfer.files;
					} else if(e.target) {
						files = e.target.files;
					}

					if(typeof nbt_solutions !== 'undefined' && nbt_solutions.customer_id != undefined){
						customer_id = nbt_solutions.customer_id;
					}else{
						customer_id = nbtou.customer_id;
					}


					var $html = '';
					for(var i=0; i<files.length; i++) {
						$html += '<div id="' + md5(customer_id + files[i].size) + '" class="nbt-file"><div class="nbt-file-left">';
						if(files[i].type.indexOf('image/') === 0) {
							$html += '<img width="50" src="' + URL.createObjectURL(files[i]) + '" />';
						}
						$html += '</div><div class="nbt-file-right">';
						$html += '<div class="name">' + files[i].name + ' <i class="nbt-icon-cancel"></i></div><div class="size"> ' + nbtou_js.calcSize(files[i].size) + '</div><div class="nbt-ou-msg" style="display: none;"></div></div>';
						$html += '</div>';
					}
					$('.nbt-oupload-body').append($html);



				  	var data = new FormData();
				  	data.append("action", "nbt_order_upload");
				  	data.append("product_id", $product_id);
				  	$.each(files, function(key, value){
				      	data.append("nbt_files[]", value);
				    });
					if(typeof nbt_solutions !== 'undefined' && nbt_solutions.ajax_url != undefined){
						ajax_url = nbt_solutions.ajax_url;
					}else{
						ajax_url = nbtou.ajax_url;
					}

					nbtou_load.block($('#nbt-order-upload'));
			    	$.ajax({
						url: ajax_url,
						type: 'POST',
						data: data,
						cache: false,
						dataType: 'json',
						processData: false, // Don't process the files
						contentType: false, // Set content type to false as jQuery will tell the server its a query string request
						xhr: function() {
						    var xhr = new window.XMLHttpRequest();
						    var started_at = new Date();
						    console.log(xhr);
						    xhr.upload.addEventListener("progress", function(evt){
						    	console.log(evt);
						      if (evt.lengthComputable) {
						      	var loaded = evt.loaded;
						      	var total = evt.total;

							var seconds_elapsed =   ( new Date().getTime() - started_at.getTime() )/1000;
							var bytes_per_second =  seconds_elapsed ? loaded / seconds_elapsed : 0 ;
							var Kbytes_per_second = bytes_per_second / 1000 ;
							var remaining_bytes =   total - loaded;
							var seconds_remaining = seconds_elapsed ? remaining_bytes / bytes_per_second : 'calculating' ;
							jQuery( '.timeRemaining' ).html( '' );
							jQuery( '.timeRemaining' ).append( Math.round(seconds_remaining) );

							$('.progress').show();
						        $('.progress').find('.progress-bar').css('width',Math.round((evt.loaded / evt.total) * 100) + "%");
						      }
						    }, false);

						    return xhr;
						},
						success: function(data, textStatus, jqXHR) {
							nbtou_load.unblock($('#nbt-order-upload'));
							$.each(data.response, function( index, value ) {
								
								if( value == "SUCCESS" ){
									$('#' + index).addClass('success');
								}else{
									$('#' + index + ' .nbt-ou-msg').show();
									$('#' + index).addClass('error');
									$('#' + index + ' .nbt-ou-msg').html(value);
								}
							});

						}
					});



				},
				click_files: function(){
					$('.nbt-upload-input').val('');
					$('.nbt-upload-input').trigger('click');
				},
				change_files: function(e){
					nbtou_js.triggerCallback(e);
				},
				calcSize: function(nBytes) {
					if (nBytes == 0) {
						return {size: '', label: ''}
					}
					for (var aMultiples = ["Kb", "Mb", "Gb", "Tb", "Pb", "Eb", "Zb", "Yb"], nMultiple = 0, nApprox = nBytes / 1024; nApprox > 1000; nApprox /= 1000, nMultiple++) {
						//sOutput = nApprox.toFixed(3) + " " + aMultiples[nMultiple] + " (" + nBytes + " bytes)";
					}
					return nApprox.toFixed(2) + ' ' + aMultiples[nMultiple];
					
				},
				remove_files: function(){
					nbtou_load.block($('#nbt-order-upload'));
					var $id = $(this).closest('.nbt-file').attr('id');
					if(typeof nbt_solutions !== 'undefined' && nbt_solutions.ajax_url != undefined){
						ajax_url = nbt_solutions.ajax_url;
					}else{
						ajax_url = nbtou.ajax_url;
					}
					$.ajax({
						url: ajax_url,
						data: {
							action:     'nbt_ou_remove',
							product_id: $('[name="add-to-cart"]').val(),
							file : $id
						},
						type: 'POST',
						datatype: 'json',
						success: function( response ) {
							nbtou_load.unblock($('#nbt-order-upload'));
							var rs = JSON.parse(response);
							if ( rs.complete != undefined ) {
								$('#' + rs.file_id).remove();
							}

							

						},
						error:function(){
							alert('There was an error when processing data, please try again !');
							nbtou_load.unblock($('#nbt-order-upload'));
						}
					});

					return false;
				}
			}
			
			nbtou_js.init();
		});	



