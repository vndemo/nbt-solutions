<?php
class NBT_Pdf_Creator_Settings{
	static $id = 'pdf';

	protected static $initialized = false;

	public static function initialize() {
		// Do nothing if pluggable functions already initialized.
		if ( self::$initialized ) {
			return;
		}


		// State that initialization completed.
		self::$initialized = true;
	}

    public static function get_settings() {
        $settings = array(

            'pdf_template' => array(
                'name' => __( 'PDF Templates', 'nbt-pdf-creator' ),
                'type' => 'radio_image',
                'id'   => 'nbt_'.self::$id.'_template',
                'default' => 'temp1',
                'option' => self::option_template()
            ),
            'logo' => array(
                'name' => __( 'Logo', 'nbt-pdf-creator' ),
                'type' => 'image',
                'id'   => 'nbt_'.self::$id.'_logo',
                'default' => 'http://netbasejsc.com/images/logo.png'
            ),
            'brandname' => array(
                'name' => __( 'Brand Name', 'nbt-pdf-creator' ),
                'type' => 'text',
                'id'   => 'nbt_'.self::$id.'_brands',
                'default' => 'Netbase JSC,.'
            ),
            'address' => array(
                'name' => __( 'Address', 'nbt-pdf-creator' ),
                'type' => 'textarea',
                'id'   => 'nbt_'.self::$id.'_address',
                'default' => 'Room A702, M3-M4 Building, 91 Nguyen Chi Thanh Str, Dong Da Dist, Hanoi, Vietnam'
            ),
            'color' => array(
                'name' => __( 'Primary Color', 'nbt-pdf-creator' ),
                'type' => 'color',
                'id'   => 'nbt_'.self::$id.'_primary_color',
                'default' => '#cd3334'
            ),
            'color_text' => array(
                'name' => __( 'Text Color', 'nbt-pdf-creator' ),
                'type' => 'color',
                'id'   => 'nbt_'.self::$id.'_text_color',
                'default' => '#000'
            )
        );
        return apply_filters( 'nbt_'.self::$id.'_settings', $settings );
    }

    public static function option_template(){
        $template_option = array(
            'temp1' => array(
                'name' => 'temp1',
                'src' => NBT_PDF_URL . 'assets/img/temp1.jpg',
                'label' => 'Nhãn'
            ),
            'temp2' => array(
                'name' => 'temp2',
                'src' => NBT_PDF_URL . 'assets/img/temp2.jpg',
                'label' => 'Nhãn'
            ),
            'temp3' => array(
                'name' => 'temp3',
                'src' => NBT_PDF_URL . 'assets/img/temp3.jpg',
                'label' => 'Nhãn'
            ),
        );
        return apply_filters( 'nbt_pdf_creator_template', $template_option );
    }

}
