<?php
/**
 * @version    1.0
 * @package    Package Name
 * @author     Your Team <support@yourdomain.com>
 * @copyright  Copyright (C) 2014 yourdomain.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 */

/**
 * Plug additional sidebars into WordPress.
 *
 * @package  Package Name
 * @since    1.0
 */
define('NBT_REG_PATH', plugin_dir_path( __FILE__ ));
define('NBT_REG_URL', plugin_dir_url( __FILE__ ));
class NBT_Solutions_Register {
	/**
	 * Variable to hold the initialization state.
	 *
	 * @var  boolean
	 */
	protected static $initialized = false;

	private static $settings_saved;

	/**
	 * Initialize functions.
	 *
	 * @return  void
	 */
	public static function initialize() {
		// Do nothing if pluggable functions already initialized.
		if ( self::$initialized ) {
			return;
		}

		self::$settings_saved = false;

		/**
		* Load modules
		*/
		add_action( 'widgets_init', array(__CLASS__, 'register_widgets') );


		include NBT_CORE_PATH.'modules/brands/inc/metabox.php';
		include NBT_CORE_PATH.'modules/brands/inc/widgets.php';
		include NBT_CORE_PATH.'modules/ajax-cart/inc/widgets.php';
		include NBT_CORE_PATH.'modules/currency-switcher/inc/widgets.php';
		
		NBT_Solutions_Brands_Metabox::initialize();

		self::$initialized = true;
	}

	public static function register_widgets(){
		register_widget( 'NBT_Brands_Thumbnail_Widget' );
		register_widget( 'NBT_Brands_Slider_Widget' );
		register_widget( 'NBT_Ajax_Cart_Widget' );
		register_widget( 'NBT_Currency_Switcher_Widget' );
		
	}

}

