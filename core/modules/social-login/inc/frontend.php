<?php
class NBT_WooCommerce_Social_Login_Frontend{
	protected $args;

    public $hasFacebook = false;
    public $hasGoogle = false;

	function __construct() {
	
		add_action('wp_enqueue_scripts', array($this, 'embed_style'));

        $nbtSetting = get_option('social-login_settings');
        $nbtFid = (isset($nbtSetting['nbt_social_login_facebook_id'])) ? $nbtSetting['nbt_social_login_facebook_id'] : '';
        $nbtFsc = (isset($nbtSetting['nbt_social_login_facebook_secret'])) ? $nbtSetting['nbt_social_login_facebook_secret'] : '';
        $nbtGid = (isset($nbtSetting['nbt_social_login_google_id'])) ? $nbtSetting['nbt_social_login_google_id'] : '';
        $nbtGsc = (isset($nbtSetting['nbt_social_login_google_secret'])) ? $nbtSetting['nbt_social_login_google_secret'] : '';

        if (($nbtFid !== '' && $nbtFsc !== '')) {
            $this->hasFacebook = true;
        }

        if ($nbtGid !== '' && $nbtGsc !== '') {
            $this->hasGoogle = true;
        }

        if (($nbtFid !== '' && $nbtFsc !== '') || ($nbtGid !== '' && $nbtGsc !== '')) {
            add_action('woocommerce_login_form', array($this, 'nbt_social_wc_myacount'));
            add_action('login_form', array($this, 'nbt_social_wc_myacount'));
        }
 
        $this->nbt_social_popup_login();

	}

    function nbt_social_popup_login()
    {



        $login = (isset($_GET['login'])) ? $_GET['login'] : '';

        if ($login !== '') {

            require_once 'check-login.php';
        }
        return;

    }

	function embed_style(){

        wp_register_style('nbt_solution_social_login', SOCIAL_LOGIN_URL . 'assets/css/social.css');
        wp_enqueue_style('nbt_solution_social_login');
	}

    function nbt_social_wc_myacount()
    {
        $output = $this->html_social_login();
        echo $output;

        return;

    }

    function html_social_login()
    {
//        $output .= '<a href="?login=facebook">';
//        $output .= '<a href="?login=google">';

        $output = '';
        $output .= '<div class="social social-wrap b">';

        if ($this->hasFacebook) {

            $output .= '<a href="?login=facebook">';
                $output .= '<button type="button" class="facebook">Sign in with Facebook</button>';
            $output .= '</a>';
        }

        if ($this->hasGoogle) {
            $output .= '<a href="?login=google">';
                $output .= '<button type="button" class="googleplus">Sign in with Google</button>';
            $output .= '</a>';
        }


        $output .= '</div>';

        return $output;
    }

}
new NBT_WooCommerce_Social_Login_Frontend();

?>
