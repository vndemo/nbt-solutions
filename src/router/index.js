import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '../pages/dashboard.vue'
// import Hello from '../pages/Hello.vue'
import Modules from '../pages/modules.vue'
import Settings from '../pages/settings.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Dashboard',
      component: Dashboard
    },
    {
      path: '/modules',
      name: 'Modules',
      component: Modules
    },
    {
      path: '/settings',
      name: 'Settings',
      component: Settings
    }
  ]
})
